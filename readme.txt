************************************************
||            CLEVERIDGE SSH SCANNER          ||
************************************************
||  IMPORTANT:                                ||
||  This tool is for ethical testing purpose  ||
||  only.                                     ||
||  Cleveridge and its owners can't be held   ||
||  responsible for misuse by users.          ||
||  Users have to act as permitted by local   ||
||  law rules.                                ||
************************************************
||     Cleveridge - Ethical Hacking Lab       ||
||               cleveridge.org               ||
************************************************

We moved to Github: https://github.com/Cleveridge/cleveridge-ssh-scanner